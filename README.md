# evan's website

**now hosted at [oicleevan.xyz](https://oicleevan.xyz)!**

## what is this?

a host for my goings on, projects, links to my other stuff, etc.

hosts documentation for my twitch bot ([repo](https://github.com/oicleevan/stylisbot)/[site](https://oicleevan.xyz/stylisbot/home.html)), talks about my awesome new game, [the deeps](https://github.com/oicleevan/the-deeps).

awesome! cool! epic!

## supporting

[![Support me at paypal.me](https://www.zahlungsverkehrsfragen.de/wp-content/uploads/2018/10/paypalme.png "Support me at paypal.me")](https://paypal.me/eoicle)

**-- [oicleevan](https://oicleevan.xyz)**


